if (document.querySelector('[name=theme-color]') !== null) {
    let mList = document.querySelector('[name=theme-color]'),
        options = {
            attributes: true
        },
        observer = new MutationObserver(mCallback);

    function mCallback(mutations) {
        for (let mutation of mutations) {
            if (mutation.type === 'attributes') {
                getThemeColor();
            }
        }
    }
    observer.observe(mList, options);
}