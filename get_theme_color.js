let getThemeColor = function () {
    if (window.matchMedia('(prefers-color-scheme: dark)').matches === true) {
        let test = document.querySelector('[name=theme-color][media="(prefers-color-scheme: dark)"]');
        if (test !== null) {
            var themeColor = test.content
            browser.runtime.sendMessage({ "color": themeColor });
        } else if (document.querySelector('[name=theme-color]:not([media])') !== null) {
            var themeColor = document.querySelector('[name=theme-color]:not([media])').content
            browser.runtime.sendMessage({ "color": themeColor });
        } else {
            browser.runtime.sendMessage({ "color": "none" });
        }
    } else {
        if (document.querySelector('[name=theme-color][media="(prefers-color-scheme: light)"]') !== null) {
            var themeColor = document.querySelector('[name=theme-color][media="(prefers-color-scheme: light)"]').content
            browser.runtime.sendMessage({ "color": themeColor });
        } else if (document.querySelector('[name=theme-color]:not([media])') !== null) {
            var themeColor = document.querySelector('[name=theme-color]:not([media])').content
            browser.runtime.sendMessage({ "color": themeColor });
        } else {
            browser.runtime.sendMessage({ "color": "none" });
        }
    }
}



document.addEventListener("DOMContentLoaded", function () {
    if (document.visibilityState === 'visible') {
        getThemeColor();
    }
});

document.addEventListener("readystatechange", function () {
    if (document.readyState === "complete" && document.visibilityState === 'visible') {
        getThemeColor();
    }
});


document.addEventListener("visibilitychange", function () {
    if (document.visibilityState === 'visible') {
        getThemeColor()
    }

});