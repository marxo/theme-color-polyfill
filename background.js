browser.runtime.onMessage.addListener(function (message) {
	// console.log(message);
	if (message.color !== "none") {
		updateTheme(message.color)
	} else {
		updateTheme("none")
	}
});

function updateTheme(color) {
	if (color !== "none") {
		browser.theme.update(browser.windows.WINDOW_ID_CURRENT, {
			colors: {
				tab_background_text: getContrast(color),
				frame: color,
				toolbar_field_border_focus: color
			}
		})
	} else {
		browser.theme.reset(browser.windows.WINDOW_ID_CURRENT)
	}
}

var getContrast = function (hexcolor) {

	if (hexcolor.includes('rgb')) {
		var rgb = hexcolor.replace(/^(rgb|rgba)\(/, '').replace(/\)$/, '').replace(/\s/g, '').split(',');
		var r = rgb[0];
		var g = rgb[1];
		var b = rgb[2];

	} else {
		// If a leading # is provided, remove it

		if (hexcolor.slice(0, 1) === '#') {
			hexcolor = hexcolor.slice(1);
		}

		// If a three-character hexcode, make six-character
		if (hexcolor.length === 3) {
			hexcolor = hexcolor.split('').map(function (hex) {
				return hex + hex;
			}).join('');
		}

		// Convert to RGB value
		var r = parseInt(hexcolor.substr(0, 2), 16);
		var g = parseInt(hexcolor.substr(2, 2), 16);
		var b = parseInt(hexcolor.substr(4, 2), 16);

	}



	// Get YIQ ratio
	var yiq = ((r * 0.2126) + (g * 0.7152) + (b * 0.114));

	// Check contrast
	return (yiq >= 128) ? 'black' : 'white';

};